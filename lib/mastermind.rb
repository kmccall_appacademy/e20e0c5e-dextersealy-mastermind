class Code
  attr_reader :pegs

  PEGS = {
    red: "r",
    green: "g",
    blue: "b",
    yellow: "y",
    orange: "o",
    purple: "p"
  }

  def initialize(pegs)
    @pegs = []
    chars = PEGS.values
    pegs.each do |chr|
      raise "invalid color" unless chars.include?(chr)
      @pegs << chr
    end
  end

  def [](index)
    pegs[index]
  end

  def exact_matches(other_code)
    pegs.each_with_index.reduce(0) do |sum, (chr, idx)|
      sum += (other_code[idx] == chr) ? 1 : 0
    end
  end

  def near_matches(other_code)
    *counters = count_mismatches(pegs, other_code)
    (counters.map {|hsh| hsh.keys}).flatten.uniq.reduce(0) do |sum, chr|
      counts = counters.map { |hsh| hsh[chr] }
      sum += counts.none?(&:nil?) ? counts.min : 0
    end
  end

  def ==(other_code)
    other_code.is_a?(Code) && exact_matches(other_code) == pegs.length
  end

  def to_str
    pegs.join.upcase
  end

  def self.parse(colors)
    Code.new(colors.downcase.chars)
  end

  def self.random()
    colors = PEGS.values
    parse(4.times.map { |i| colors.sample }.join)
  end

  private

  def count_mismatches(seq_a, seq_b)
    hash_a, hash_b = Hash.new(0), Hash.new(0)

    seq_a.each_with_index do |el_a, idx|
      el_b = seq_b[idx]
      next if el_b == el_a
      hash_a[el_a] += 1
      hash_b[el_b] += 1
    end

    return hash_a, hash_b
  end

  def min(a, b)
    a < b ? a : b
  end

end

class Game
  attr_reader :secret_code

  def initialize(code=nil)
    @secret_code = code ? code : Code.random
  end

  def get_guess
    print "Enter your guess: "
    input = gets.chomp
    Code.parse(input)
  end

  def display_matches(code)
    exact_matches = secret_code.exact_matches(code)
    near_matches = secret_code.near_matches(code)
    puts "You got #{near_matches} near and #{exact_matches} exact."
  end

  def play
    remaining_guesses = 10
    while remaining_guesses > 0

      guess = get_guess
      remaining_guesses -= 1

      if secret_code == guess
        puts "Correct!"
        break
      end

      puts "Wrong!"
      display_matches(guess)
    end

    puts "The secret code was #{secret_code.to_str}"
  end

end

if __FILE__ == $PROGRAM_NAME
  Game.new.play
end
